#!/usr/bin/env python2.7

import os
import socket
import sys
import getopt

# Constants

REQS = 1
PROCS = 1
VERB = False
ADDRESS = '127.0.0.1'
PORT    = 80
PROGRAM = os.path.basename(sys.argv[0])

# Functions

def usage():
	print 'Usage: thor.py [-r REQS -p PROCS -v] URL \n \nOptions:\n\n\t-h\t\tShow this help message\n\t-v\t\tSett logging to DEBUG level\n\n\t-r REQS\tNumber of requests per process (default is 1)\n\t-p PROCS\tNumber of processes (default is 1)'
	return 1
	
# Echo Client

def main():
	try:
		options, args = getopt.getopt(sys.argv[1:], "r:p:v")
	except getopt.GetoptError as e:
		usage()
		sys.exit(1)
	
	for option, value in options:
		if option == "-r":
			REQS = value
		elif option == "-p":
			PROCS = value
		elif option == "-v":
			VERB = True
		else:
			usage()
	
	ADDRESS = sys.argv[len(sys.argv)-1]

	try:
		ADDRESS = socket.gethostbyname(ADDRESS)
	except socket.gaierror as e:
		print 'Unable to find hostname for {}'.format(ADDRESS)
		sys.exit(1)
		 
	client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Allocate TCP socket
	print ADDRESS, PORT
	client.connect((ADDRESS, PORT))       # Connect to server with ADDRESS and PORT
	data = '1'
	while data:
		client.send("GET / HTTP/1.0\r\n\r\n")
		data = ''
		data = (client.recv(500))
		sys.stdout.write(data)
	
	client.shutdown(1)
	client.close()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
if __name__ == "__main__":
	main()
