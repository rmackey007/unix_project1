#!/usr/bin/env python2.7

import socket

DOMAIN = 'www.example.com'
PORT = 80
#try:
#address = socket.gethostbyname(DOMAIN)
#except some kind of error:
#	print 'Cannot get IP Address'
#	return 1
#try:
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
						  #IPv4			   TCP
#except another error:
#	print 'Socket Error'
#	return 1
server.bind(('0.0.0.0', 9999))
server.listen(0)
client, address = server.accept()
print 'Connection from ', address
stream = client.makefile('w+')  #convert file descriptor into file object
data = stream.readline().rstrip()
while data:
	print data
	data = stream.readline().rstrip()
stream.write('HTTP/1.0 200 OK')
