#!/usr/bin/env python2.7

import socket

DOMAIN = 'www.example.com'
PORT = 80
#try:
address = socket.gethostbyname(DOMAIN)
#except some kind of error:
#	print 'Cannot get IP Address'
#	return 1
#try:
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
						  #IPv4			   TCP
#except another error:
#	print 'Socket Error'
#	return 1
client.connect((address, PORT))
stream = client.makefile('w+')  #convert file descriptor into file object
stream.write('GET / HTTP/1.0\r\n\r\n')
stream.flush()
data = stream.readline()
while data:
	print data
	data = stream.readline()
